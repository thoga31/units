{$mode objfpc}
program test1;
uses classes, sysutils,
     UOrder;

type TOrdInt = specialize TOrdering<integer>;
     TArrInt = array of integer;


function Greater(a, b : integer) : boolean;
begin
   Greater := a > b;
end;


procedure WriteArrInt(arr : TArrInt);
var i : integer;
begin
   for i in arr do
      write(i:3);
   writeln;
end;


var ordint : TOrdInt;
    l      : TArrInt;

begin
   try
      SetLength(l, 6);
      l[0] := 5;
      l[1] := 5;
      l[2] := 0;
      l[3] := 4;
      l[4] := 2;
      l[5] := 8;

      write('Initial array: ');
      WriteArrInt(l);
      writeln;

      ordint.Create;
      ordint.Sort(l, @Greater);
      ordint.Free;

      write('  Final array: ');
      WriteArrInt(l);

   except
      on ex:exception do writeln('[ERR: ', ex.classname, ' -> ', ex.message, ']');
   end;

   readln;  // pausa
end.