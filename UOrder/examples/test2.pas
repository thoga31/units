{$mode objfpc}
program test2;
uses classes, sysutils,
     UOrder;

type TOrdStr = specialize TOrdering<string>;
     TArrStr = array of string;


function Max(x, y : word) : word;
begin
   if x >= y then
      Max := x
   else
      Max := y;
end;


function AlphaBefore(p1, p2 : string) : boolean;
// Is p1 before p2 in the dictionary? It may not work with spaces!
var i : word;
begin
   AlphaBefore := true;
   for i := 1 to Max(Length(p1), Length(p2)) do
      if Ord(LowerCase(p1[i])) < Ord(LowerCase(p2[i])) then
         break
      else if Ord(LowerCase(p1[i])) > Ord(LowerCase(p2[i])) then begin
         AlphaBefore := false;
         break;
      end;
end;


procedure WriteArr(arr : TArrStr);
var s : string;
begin
   for s in arr do writeln(s);
   writeln;
end;


var ordstr : TOrdStr;
    names : TArrStr;

begin
   try
      SetLength(names, 4);
      names[0] := 'thomas';
      names[1] := 'john';
      names[2] := 'adam';
      names[3] := 'sophie';

      writeln('Initial array:');
      WriteArr(names);
      writeln;

      ordstr.Create;
      ordstr.Sort(names, @AlphaBefore);
      ordstr.Free;

      writeln('Final array:');
      WriteArr(names);

   except
      on ex:exception do writeln('[ERR: ', ex.classname, ' -> ', ex.message, ']');
   end;

   readln;
end.