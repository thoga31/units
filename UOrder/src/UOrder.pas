{$mode objfpc}
unit UOrder;

interface
uses classes, sysutils;

type generic TOrdering<T> = class(TObject)
        private
           type TFnBool = function (a, b : T) : boolean;
                TArrayOfT = array of T;
        public
           procedure Sort(var arr : TArrayOfT; f : TFnBool);
     end;


implementation

procedure TOrdering.Sort(var arr : TArrayOfT; f : TFnBool);
var i, j : longint;
    temp : T;
begin
   for i := Low(arr) to High(arr)-1 do
      for j := i+1 to High(arr) do
         if f(arr[i], arr[j]) then begin
            temp := arr[i];
            arr[i] := arr[j];
            arr[j] := temp;
         end;
end;

end.