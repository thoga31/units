(* * * * * * * * * * * * * * * === UNIT  debug === * * * * * * * * * * * * * * *
 *      Version: 1.0.0                                                         *
 * Release date: November 1st, 2015                                            *
 *      License: WTFPL                                                         *
 *       Author: Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt         *
 *                                                                             *
 *  Description: Class for easy Log Files. Multiple instances allow multiple   *
 *               types of Log Files with different properties.                 *
 *                                                                             *
 * Implemented, compiled and tested with Free Pascal Compiler 3.0.0rc1         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

{$mode objfpc}
unit debug;

interface
uses classes, dateutils, sysutils, strutils;

type
   TLogFile = class(TStringList)
      private
         const
            DIR_DEFAULT = 'log';    // Default directory where the Log Files are saved.
         
         var
            fname : string;         // File name of the Log File for the present session.
                                    // It is automatically generated when the instance is created.
         
         // Date and time formatting
         function DateToStr(d : TDateTime) : string;
         function FormatTimeForLogFile(d : TDateTime) : string;
         
         // For the registry of variables values
         procedure RegVarFirstLine(const VARNAME : string; thevalue : string = '');
      
      public
         constructor Create(const PREFIX : string; const DIR : string = DIR_DEFAULT);
         
         procedure RegEvent(eventclass, eventdescription : string);
         
         procedure RegVar(const VARNAME : string; v : LongInt);
         procedure RegVar(const VARNAME : string; v : Double); overload;
         procedure RegVar(const VARNAME : string; s : string); overload;
         procedure RegVar(const VARNAME : string; list : TStringList); overload;
   end;



implementation

(* AUXILIARY TOOLS for the TLogFile class methods *)

function Int2Str(i : LongInt; size : word) : string;
(* Extension of the IntToStr function: adds trailing zeros given the string's minimum 'size'. *)
begin
   Int2Str := IntToStr(i);
   if Length(Int2Str) < size then
      Int2Str := DupeString('0', size - Length(Int2Str)) + Int2Str;
end;


(* PUBLIC methods *)

constructor TLogFile.Create(const PREFIX : string; const DIR : string = DIR_DEFAULT);
(* If DIR isn't specified, the Log File wiil be saved in the default folder.
   The directory is automatically created if it doesn't exist.               *)
begin
   self.fname := IfThen(DIR <> '', DIR + '/', '') + PREFIX + '_' + self.FormatTimeForLogFile(Now) + '.txt';
   if (DIR <> '') and not DirectoryExists(self.DIR_DEFAULT) then
      CreateDir(self.DIR_DEFAULT);
   inherited Create;
end;


procedure TLogFile.RegEvent(eventclass, eventdescription : string);
(* Logs an event. 'eventclass' will be capitalized. *)
var line : string = '';
begin
   line := '(' + DateToStr(Now) + ') ' + UpCase(eventclass) + ': ' + eventdescription;
   self.Add(line);
   self.SaveToFile(self.fname);
end;


procedure TLogFile.RegVar(const VARNAME : string; v : LongInt);
(* Logs an integer variable value. *)
begin
   self.RegVarFirstLine(VARNAME, IntToStr(v));
   self.SaveToFile(self.fname);
end;


procedure TLogFile.RegVar(const VARNAME : string; v : Double); overload;
(* Logs a number variable value. *)
begin
   self.RegVarFirstLine(VARNAME, FloatToStrF(v, ffNumber, 10, 10));  // Includes thousand separator.
   self.SaveToFile(self.fname);
end;


procedure TLogFile.RegVar(const VARNAME : string; s : string); overload;
(* Logs a string variable value. *)
begin
   self.RegVarFirstLine(VARNAME, '''' + s + '''');
   self.SaveToFile(self.fname);
end;


procedure TLogFile.RegVar(const VARNAME : string; list : TStringList); overload;
(* Logs a TStringList instance content. *)
var s : string;
begin
   self.RegVarFirstLine(VARNAME);
   for s in list do
      self.Add('   ' + s);
   self.SaveToFile(self.fname);
end;


(* PRIVATE methods *)

function TLogFile.DateToStr(d : TDateTime) : string;
begin
   DateToStr := IntToStr(YearOf(d)) + '-' + Int2Str(MonthOf(d), 2) + '-' + Int2Str(DayOf(d), 2) + ' ' +
                Int2Str(HourOf(d), 2) + ':' + Int2Str(MinuteOf(d), 2) + ':' + Int2Str(SecondOf(d), 2);
end;


function TLogFile.FormatTimeForLogFile(d : TDateTime) : string;
begin
   FormatTimeForLogFile := IntToStr(YearOf(d)) + Int2Str(MonthOf(d), 2) + Int2Str(DayOf(d), 2) + '_' +
                           Int2Str(HourOf(d), 2) + Int2Str(MinuteOf(d), 2) + Int2Str(SecondOf(d), 2);
end;


procedure TLogFile.RegVarFirstLine(const VARNAME : string; thevalue : string = '');
begin
   self.Add('(' + DateToStr(Now) + ') ' + 'Variable ' + VARNAME + ' is now:' +
            IfThen(thevalue = '', '', ' ' + thevalue));
end;

end.