Unit debug

1.0.0

Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt

Licensed under the WTFPL License (http://www.wtfpl.net/about/)

Language: Object Pascal (using Free Pascal Compiler, only)

This unit offers a class with static methods to easily manipulate open arrays of any type,
as well as a bunch of operator overloads for open arrays of SmallInt.