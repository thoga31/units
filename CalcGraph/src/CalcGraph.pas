(* === UNIT CalcGraph ===
 * Tool to draw graphics of functions.
 *
 *    Author: Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt
 *      Date: July 2012
 *   Version: 1.0.0 (beta-1)
 * Traget OS: Windows(c)
 * 
 * Licensed under the GNU GPL 3.0 (https://www.gnu.org/copyleft/gpl.html)
 * 
 * For "Revista PROGRAMAR", 36th edition (August, 2012) *)

{$MODE objfpc}
UNIT CalcGraph;

INTERFACE
USES graph, types;

TYPE TRealFunction = FUNCTION(x:real):real;

     TZoom = PACKED RECORD
        XMin, XMax, XScl : real;
        YMin, YMax, YScl : real;
     END;

     TLineStyle = PACKED RECORD
        Color : word;
        Thickness : word;
        Style : word;
        Visible : boolean;
     END;

     TPlotter = CLASS(TObject)
        PRIVATE
           vPosition : TPoint;
           vSize : TSize;
           vZoom : TZoom;

           vCenter : TPoint;
           DistBetweenGrid : TPoint;

           vFunction : TRealFunction;
           vValuesOfFunction : array [0..2000] of real;

           procedure CalcDistGridAndCenter;

        PUBLIC
           Grid : TLineStyle;
           Axes : TLineStyle;
           FunctionLine : TLineStyle;

           BackColor : word;

           property Position : TPoint read vPosition;
           property Size : TSize read vSize;
           property Zoom : TZoom read vZoom;
           property Center : TPoint read vCenter;

           procedure Show;
           procedure Clear;
           procedure Refresh;

           procedure Relocate(cNewPosition : TPoint);
           procedure Relocate(cX, cY : LongInt); overload;

           procedure Resize(cNewSize : TSize);
           procedure Resize(ccx, ccy : LongInt); overload;

           procedure Rezoom(cNewZoom : TZoom);

           procedure LoadFunction(fn : TRealFunction);
           procedure DisposeFunction(cClearPlotter : boolean);
           procedure DrawFunction;
           procedure ClearFunction;

           constructor Create(cPosition : TPoint; cSize : TSize);
           constructor Create(cPosition : TPoint; cSize : TSize; cZoom : TZoom;
                              cGrid, cAxes, cFnLine : TLineStyle; cBkColor : word); overload;
     END;




CONST ZoomStandard : TZoom = (XMin:-10; XMax:10; XScl:1; YMin:-10; YMax:10; YScl:1);
      DefaultGrid : TLineStyle = (Color:DarkGray; Thickness:NormWidth; Style:DashedLn; Visible:True);
      DefaultAxes : TLineStyle = (Color:White; Thickness:ThickWidth; Style:SolidLn; Visible:True);
      DefaultFunctionLine : TLineStyle = (Color:Red; Thickness:ThickWidth; Style:SolidLn; Visible:True);
      DefaultPlotterBackColor : word = Green;



IMPLEMENTATION

PROCEDURE TPlotter.CalcDistGridAndCenter;
begin
     self.DistBetweenGrid.X := Round(self.Zoom.XScl * (self.Size.cx / (self.Zoom.XMax - self.Zoom.XMin)));
     self.DistBetweenGrid.Y := Round(self.Zoom.YScl * (self.Size.cy / (self.Zoom.YMax - self.Zoom.YMin)));

     // Calculation of the center of the plotter
     if (self.Zoom.XMin < 0) and (self.Zoom.XMax > 0) then begin
        self.vCenter.X := round(self.Position.X + abs(self.Zoom.XMin) * (self.DistBetweenGrid.X / self.Zoom.XScl));
     end else begin
         if (self.Zoom.XMin < 0) and (self.Zoom.XMax < 0) then
            self.vCenter.X := self.Position.X + self.Size.cx
         else self.vCenter.X := self.Position.X;
     end;

     if (self.Zoom.YMin < 0) and (self.Zoom.YMax > 0) then begin
        self.vCenter.Y := round(self.Position.Y + abs(self.Zoom.YMax) * (self.DistBetweenGrid.Y / self.Zoom.YScl));
     end else begin
         if (self.Zoom.YMin < 0) and (self.Zoom.YMax < 0) then
            self.vCenter.Y := self.Position.Y + self.Size.cy
         else self.vCenter.Y := self.Position.Y;
     end;
end;


CONSTRUCTOR TPlotter.Create(cPosition : TPoint; cSize : TSize);
begin
     self.Create(cPosition, cSize, ZoomStandard, DefaultGrid, DefaultAxes, DefaultFunctionLine, DefaultPlotterBackColor);
end;


CONSTRUCTOR TPlotter.Create(cPosition : TPoint; cSize : TSize; cZoom : TZoom;
                              cGrid, cAxes, cFnLine : TLineStyle; cBkColor : word); OVERLOAD;
begin
     self.vPosition := cPosition;
     self.vSize := cSize;
     self.vZoom := cZoom;
     self.Grid := cGrid;
     self.Axes := cAxes;
     self.FunctionLine := cFnLine;
     self.vFunction := nil;

     self.BackColor := cBkColor;

     self.CalcDistGridAndCenter;
end;


PROCEDURE TPlotter.Show;
var counter : integer;
begin
     // Fills background
     SetFillStyle(SolidFill, self.BackColor);
     Bar(self.Position.X, self.Position.Y, self.Position.X + self.Size.cx, self.Position.Y + self.Size.cy);

     // Draws grid, if visible
     if self.Grid.Visible then begin
        SetColor(self.Grid.Color);
        SetLineStyle(self.Grid.Style, 0, self.Grid.Thickness);

        counter := round(self.Zoom.XMin);
        repeat
              Line(self.Center.X + counter * self.DistBetweenGrid.X, self.Position.Y, self.Center.X + counter * self.DistBetweenGrid.X, self.Position.Y + Self.Size.cy);
              Inc(counter, 1);
        until counter >= self.Zoom.XMax;

        counter := round(self.Zoom.YMin);
        repeat
              Line(self.Position.X, self.Center.Y - counter * self.DistBetweenGrid.Y, self.Position.X + self.Size.cx, self.Center.Y - counter * self.DistBetweenGrid.Y);
              Inc(counter, 1);
        until counter >= self.Zoom.YMax;
     end;

     if self.Axes.Visible then begin
        SetColor(self.Axes.Color);
        SetLineStyle(self.Axes.Style, 0, self.Axes.Thickness);
        Line(self.Center.X, self.Position.Y, self.Center.X, self.Position.Y + self.Size.cy);
        Line(self.Position.X, self.Center.Y, self.Position.X + self.Size.cx, self.Center.Y);
     end;
end;


PROCEDURE TPlotter.Clear;
begin
     SetFillStyle(EmptyFill, Black);
     Bar(self.Position.X, self.Position.Y, self.Position.X + self.Size.cx, self.Position.Y + self.Size.cy);
end;


PROCEDURE TPlotter.Refresh;
begin
     self.Clear;
     self.Show;
end;


PROCEDURE TPlotter.Relocate(cNewPosition : TPoint);
begin
     self.Clear;
     self.vPosition := cNewPosition;
     self.Show;
end;


PROCEDURE TPlotter.Relocate(cX, cY : LongInt); OVERLOAD;
var NewPos : TPoint;
begin
     NewPos.X := cX;
     NewPos.Y := cY;
     self.Relocate(NewPos);
end;


PROCEDURE TPlotter.Resize(cNewSize : TSize);
begin
     self.vSize := cNewSize;

     self.CalcDistGridAndCenter;

     if not(self.vFunction = nil) then
        self.LoadFunction(self.vFunction);

     self.Refresh;
end;


PROCEDURE TPlotter.Resize(ccx, ccy : LongInt); OVERLOAD;
var NewSize : TSize;
begin
     NewSize.cx := ccx;
     NewSize.cy := ccy;
     self.Resize(NewSize);
end;


PROCEDURE TPlotter.ReZoom(cNewZoom : TZoom);
begin
     self.vZoom := cNewZoom;

     self.CalcDistGridAndCenter;

     if not(self.vFunction = nil) then
        self.LoadFunction(self.vFunction);

     self.Refresh;
end;


PROCEDURE TPlotter.LoadFunction(fn : TRealFunction);
var abscissa : real;
    counter : integer;
    step : real;
begin
     self.vFunction := fn;

     counter := self.Position.X;
     abscissa := self.Zoom.XMin;
     step := self.Zoom.XScl / self.DistBetweenGrid.X;

     repeat
           self.vValuesOfFunction[counter] := self.vCenter.Y - self.vFunction(abscissa) * (self.DistBetweenGrid.Y / self.Zoom.YScl);
           abscissa := abscissa + step;
           Inc(counter, 1);
     until counter >= self.Position.X + self.Size.cx;
end;


PROCEDURE TPlotter.DisposeFunction(cClearPlotter : boolean);
var counter : integer;
begin
     self.vFunction := nil;
     for counter:=0 to 2000 do self.vValuesOfFunction[counter] := 0;

     if cClearPlotter then self.ClearFunction;
end;


PROCEDURE TPlotter.DrawFunction;
var counter : integer;
begin
     if self.FunctionLine.Visible and not(self.vFunction = nil) then begin
        SetLineStyle(self.FunctionLine.Style, 0, self.FunctionLine.Thickness);
        SetColor(self.FunctionLine.Color);

        for counter := self.Position.X to (self.Position.X + self.Size.cx - 2) do begin
            if not ((round(self.vValuesOfFunction[counter+1]) < self.Position.Y) or
               (round(self.vValuesOfFunction[counter+1]) > self.Position.Y + self.Size.cy)) then begin
                 Line(counter, round(self.vValuesOfFunction[counter]), counter+1, round(self.vValuesOfFunction[counter+1]));
            end;
        end;
     end;
end;


PROCEDURE TPlotter.ClearFunction;
begin
     self.FunctionLine.Visible := False;
     self.Refresh;
     self.FunctionLine.Visible := True;
end;



END.
