(* === UNIT CalcGraph - PROGRAM TEST ===
Igor Nunes
July, 2012
*)

{$MODE objfpc}
PROGRAM Teste;
USES Graph, CalcGraph in 'Unit', Windows, sysutils;

VAR Driver, Modus : SmallInt;
    Plot : TPlotter;

CONST InitPos : TPoint = (X:10; Y:10);
      InitSize : TSize = (cx:400; cy:200);
      PosZoom : TZoom = (XMin:-4; XMax:8; XScl:2; YMin:-9; YMax:2; YScl:1);

FUNCTION f(x : real) : real;
begin
     f := sqr(x)-8;
end;


BEGIN
   TRY
      TRY
         DetectGraph(Driver, Modus);
         InitGraph(Driver, Modus, '');

         Plot := TPlotter.Create(InitPos, InitSize);

         Plot.BackColor := Blue;
         Plot.Grid.Color := Black;
         Plot.Axes.Color := Yellow;
         Plot.Axes.Thickness := NormWidth;

         Plot.Show;

         Plot.LoadFunction(@f);

         Plot.FunctionLine := DefaultFunctionLine;
         Plot.FunctionLine.Color := LightRed;

         Plot.DrawFunction;

         readln; // pause

         Plot.Resize(700,400);
         Plot.ReZoom(PosZoom);
         Plot.DrawFunction;

      EXCEPT
         ON ex:exception do begin
            MessageBox(0, @ex.message[1], 'Erro!', 0 + MB_ICONASTERISK);
         end;
      END
   FINALLY
      write('FIM.'); readln;
      Plot.Free;
      CloseGraph;
   END
END.
