Unit CalcGraph for Object Pascal (for Windows, only)

1.0.0

Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt

Licensed under the GNU GPL 3.0 (https://www.gnu.org/copyleft/gpl.html)

Language: Object Pascal (using Free Pascal Compiler, only)

This unit allows the creation of costumable graphical calculators.
