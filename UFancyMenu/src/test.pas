(* Program to test the unit 'menu'. *)

{$mode objfpc}
program test;
uses crt, sysutils, strutils,
     UFancyMenu;

procedure Pause;
(* Pauses the program until ENTER is pressed *)
begin
    repeat
    until readkey = sys.KEY_ENTER;
end;

procedure Proc1;
begin
    clrscr;
    writeln('Proc1');
    Pause;
    clrscr;
end;

procedure Proc2;
begin
    clrscr;
    writeln('Proc2');
    Pause;
    clrscr;
end;


var main_menu : TMenu;

begin
    main_menu := TMenu.Create();
    with main_menu do begin
        Add('  1 > Proc1', '1', @Proc1);
        Add('  2 > Procedure 2', '2', @Proc2);
        Add('ESC > Exit', sys.KEY_ESC, nil);
        render := '+-+�| #TITLE@CENTER |�+-+�| #OPTION |�+-+';
    end;
    
    repeat
        main_menu.Show('Choose an option:');
    until main_menu.GetChoice = sys.KEY_ESC;
    
    main_menu.destroy;
end.
