{$mode objfpc}
program test;
uses sysutils, UMath;


function foo(n : integer) : integer;
begin
   foo := n*2;
end;


var i : integer;
    a : specialize TArrayHandler<integer>;  // It does NOT need an instance!
    l : array of integer;

begin
   try
      writeln('Creating list L...');
      for i:=0 to 2 do
         a.AddToArray(l, succ(i));
      
      writeln('Mapping list L...');
      l := a.Map(L, @foo);
      
      writeln('Contents of list L:');
      for i in l do
         writeln('   ', i);
      
      writeln('End.');
      
   except
      on ex : Exception do 
         writeln('ERROR ', ex.classname, ', ', ex.message);
   end;
   
   write('Enter to close...');
   readln;
end.